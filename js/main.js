function calcular(){
    const doce=1.125, dieciocho=1.172, veinticuatro=1.21, treintayseis=1.26, cuarentayocho=1.45;
    valAuto = document.getElementById('valorAutomovil').value;
    valEnganche = document.getElementById('enganche');
    valFinanciar = document.getElementById('financiar');
    valPlanCred = document.getElementById('tabla').value;
    valPagoMensual = document.getElementById('pagoMensual')

    valEnganche.innerHTML = valAuto*0.30;
    valFinanciar.innerHTML = ((valAuto - valEnganche.innerHTML)  * doce).toFixed(2);

    if(valPlanCred == 12){              
        valPagoMensual.innerHTML = (valFinanciar.innerHTML / 12).toFixed(2);
    }else if(valPlanCred == 18){
        valPagoMensual.innerHTML = (valFinanciar.innerHTML / 18).toFixed(2);
    }else if(valPlanCred == 24){
        valPagoMensual.innerHTML = (valFinanciar.innerHTML / 24).toFixed(2);
    }else if(valPlanCred == 36){
        valPagoMensual.innerHTML = (valFinanciar.innerHTML / 36).toFixed(2);
    }else if(valPlanCred == 48){
        valPagoMensual.innerHTML = (valFinanciar.innerHTML / 48).toFixed(2);
    }
}

function limpiar(){
    valAuto = document.getElementById('valorAutomovil');
    valEnganche = document.getElementById('enganche');
    valFinanciar = document.getElementById('financiar');
    valPagoMensual = document.getElementById('pagoMensual')

    valAuto.value="";
    valEnganche.innerHTML="";
    valFinanciar.innerHTML="";
    valPagoMensual.innerHTML="";
}